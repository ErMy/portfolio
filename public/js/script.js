let btn = document.querySelector('#contact-btn');
btn.addEventListener('click', showEmail);

function showEmail () {
    btn.firstElementChild.innerHTML = "<b>Email</b>: e.mylenbusch@gmx.de";
    btn.classList.remove('btn', 'btn-outline-light', 'btn-lg', 'shadow-nohover')
    btn.classList.add('btn-text', 'shadow-txt');
    btn.disabled = true;

    btn.removeEventListener('click', showEmail);
}